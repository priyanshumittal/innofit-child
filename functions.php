<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'bootstrap' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

function innofitchild_theme_setup() {
    load_child_theme_textdomain( 'innofit-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'innofitchild_theme_setup' );

function innofit_child_get_parent_options() {
	$innofit_mods = get_option( 'theme_mods_innofit' );
	if ( ! empty( $innofit_mods ) ) {
		foreach ( $innofit_mods as $innofit_mod_k => $innofit_mod_v ) {
			set_theme_mod( $innofit_mod_k, $innofit_mod_v );
		}
	}
}
add_action( 'after_switch_theme', 'innofit_child_get_parent_options' );

// END ENQUEUE PARENT ACTION
